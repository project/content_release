<?php

namespace Drupal\content_release\Access;

use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\taxonomy\TermInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Defines a schedule release access checker.
 */
class ScheduleReleaseAccess implements AccessInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a ScheduleReleaseAccess object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Schedule release access.
   *
   * @param \Drupal\taxonomy\TermInterface $taxonomy_term
   *   The taxonomy term.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(TermInterface $taxonomy_term) {
    $vid = $taxonomy_term->bundle();

    /** @var \Drupal\taxonomy\VocabularyInterface $vocabulary */
    $vocabulary = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->load($vid);

    return $vocabulary->getThirdPartySetting('content_release', 'enabled', FALSE) ? AccessResult::allowed() : AccessResult::forbidden();
  }

}
