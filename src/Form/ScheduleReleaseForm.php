<?php

namespace Drupal\content_release\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\taxonomy\TermInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Contains a ScheduleReleaseForm form.
 */
class ScheduleReleaseForm extends FormBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new ScheduleReleaseForm.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager service.
   */
  public function __construct(Connection $connection, EntityTypeManagerInterface $entity_type_manager) {
    $this->connection = $connection;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'content_release_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, TermInterface $taxonomy_term = NULL) {
    $entity_index = $this->getEntitiesToRelease($taxonomy_term->id());

    $form['info'] = [
      '#prefix' => '<div>',
      '#markup' => $this->t('Make sure that publishing option is enabled for a content type you would like to schedule for releasing, otherwise - release scheduling will be skipped for those bundles.'),
      '#suffix' => '</div>',
    ];
    $form['term'] = [
      '#type' => 'value',
      '#value' => $taxonomy_term,
    ];
    $form['entity_index'] = [
      '#type' => 'value',
      '#value' => $entity_index,
    ];
    $form['schedule_release'] = [
      '#type' => 'submit',
      '#disabled' => !$entity_index,
      '#value' => $this->formatPlural(
        count($entity_index),
        'Schedule 1 item to be released.',
        'Schedule @count items to be released.'
      ),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $publish_time = $form_state->getValue('term')->get('content_release')->value;
    $entity_index = $form_state->getValue('entity_index');
    $module_path = \Drupal::service('extension.list.module')
      ->getPath('content_release');

    $batch = [
      'title' => $this->t('Scheduling release for content...'),
      'init_message' => $this->t('Calculating'),
      'progress_message' => $this->t('Processed @current out of @total.'),
      'error_message' => $this->t('An error occurred during processing'),
      'operations' => [],
      'finished' => ['content_release_batch_finished'],
      'file' => $module_path . '/content_release.batch.inc',
    ];
    foreach ($entity_index as $row) {
      $batch['operations'][] = [
        'content_release_batch_process', [$publish_time, $row],
      ];
    }
    batch_set($batch);
  }

  /**
   * Get the entities to release.
   *
   * @param int $tid
   *   The taxonomy term ID.
   *
   * @return array
   *   Array of row objects from taxonomy_entity_index.
   */
  public function getEntitiesToRelease(int $tid) {
    // We rely on the taxonomy_entity_index module to index all of our entities.
    // There is not currently a method in that module to do this.
    $query_results = $this->connection->select('taxonomy_entity_index', 'tei')
      ->fields('tei')
      ->condition('tei.tid', $tid, '=')
      ->orderBy('tei.entity_type', 'ASC')
      ->execute();
    // Organize results.
    foreach ($query_results as $row) {
      $entity_index[] = $row;
    }
    return $entity_index;
  }

}
