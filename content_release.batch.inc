<?php

/**
 * @file
 * Contains the callbacks for the content_release batch process.
 */

/**
 * Implements callback_batch_operation().
 *
 * Executes a batch operation for release scheduling.
 *
 * @param string $publish_time
 *   The datetime value to use for scheduler.
 * @param object $row
 *   An array of entity idex items from the DB query.
 */
function content_release_batch_process(string $publish_time, object $row) {
  /** @var Drupal\Core\Entity\ContentEntityBase $entity */
  // Load the entity from the DB row.
  $entity = \Drupal::entityTypeManager()
    ->getStorage($row->entity_type)
    ->load($row->entity_id);
  // Update the scheduler field.
  $entity->set('publish_on', $publish_time);
  // Save the entity.
  $entity->save();
}

/**
 * Implements callback_batch_finished().
 *
 * Reports the 'finished' status of batch operation.
 *
 * @param bool $success
 *   A boolean indicating whether the batch mass update operation successfully
 *   concluded.
 * @param string[] $results
 *   An array of rendered links to nodes updated via the batch mode process.
 * @param array $operations
 *   An array of function calls (not used in this function).
 */
function content_release_batch_finished($success, array $results, array $operations) {
  if ($success) {
    \Drupal::messenger()->addStatus(
      t('The release scheduling has been performed.')
    );
  }
  else {
    \Drupal::messenger()->addError(
      t('An error occurred and processing did not complete.')
    );
    $message = \Drupal::translation()->formatPlural(
      count($results),
      '1 item successfully processed:', '@count items successfully processed:',
    );
    $item_list = [
      '#theme' => 'item_list',
      '#items' => $results,
    ];
    $message .= \Drupal::service('renderer')->render($item_list);
    \Drupal::messenger()->addStatus($message);
  }
}
